
public class Circulo implements IFiguraBiDimensional {
		private double raio;
	
	private boolean condicaoExistencia(double r) {
		return  r > 0 ;
	}
	
	public Circulo() {
		this.raio = 1;
	}

	public Circulo(int raio) {
		
		if (!condicaoExistencia(raio)) {
			throw new RuntimeException("Impossível construir circulo.");
		}
		
		this.raio = raio;
	}


	public double getRaio() {
		return raio;
	}

	public void setRaio(double raio) {
		if(condicaoExistencia(raio)) {
			this.raio = raio;
		}
	}

	public double perimetro() {
		return 2 * Math.PI * raio;	
	}

	public double area() {
		return Math.PI * Math.pow (raio, 2);
	}

	@Override
	public String toString() {
		return "(" + this.raio + ")";
	}


}
