public class Gerenciador {
	
	public static void main (String [] args) {
		
		IConstrucaoFabrica cf;
		
		cf = new ConstrutorFabrica();
		
		IFabricaDeCarros f;
		
		f = cf.criar("civic");
		
		System.out.println(f);
		
	}

}
