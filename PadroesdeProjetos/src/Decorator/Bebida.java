package Decorator;

public class Bebida implements IBebida{
	
	@Override
	public double valor() {
		return 2.1;
	}

	@Override
	public String descricao() {
		return "Agua";
	}
	
	@Override
	public String toString() {
		return "Valor: R$ " + valor() + " - " + descricao();
	}

}
