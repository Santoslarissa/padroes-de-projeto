package Decorator;

public abstract class Complemento implements IBebida {
	
	IBebida b;
	
	public Complemento(IBebida b) {
		this.b = b;
	}
	
	@Override
	public double valor() {
		return b.valor();
	}
	
	public abstract String descricao();

}
