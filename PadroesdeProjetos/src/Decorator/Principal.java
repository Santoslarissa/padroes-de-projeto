package Decorator;
public class Principal {
	public static void main(String[] args) {
	
	IBebida b = new Bebida();
	System.out.println(b);
	
	IBebida l = new Limao(b);
	System.out.println(l);			
	
	IBebida g = new Gelo(l);
	System.out.println(g);
	}
	
}
