
public class Triangulo implements IFiguraBiDimensional{
	private int ladoA;
	private int ladoB;
	private int ladoC;

	 
	public Triangulo () {
		this.ladoA = 1 ;
		this.ladoB = 1 ;
		this.ladoC = 1 ;
	}
	
	private boolean condicaoDeExistencia (int a,int b,int c) {
		return Math.abs(b - c) < a && a < b + c ;
	}
	
	public Triangulo (int a, int b, int c) {
		this();
		
		if (condicaoDeExistencia(a,b,c)) {
			this.ladoA = a;
			this.ladoB = b;
			this.ladoC = c;
		}
		
	}
	
	
	public int getLadoA() {
		return ladoA;
	}

	public void setLado(int a) {
		if (condicaoDeExistencia(a, ladoB, ladoC))
		this.ladoA = a;
	}

	public int getLadoB() {
		return ladoB;
	}

	public void setLado(int a, int b) {
		if (condicaoDeExistencia(a,ladoB, ladoC)) {
			this.ladoA = a;
	   }else
			if(condicaoDeExistencia(ladoA, b , ladoC)) {
				this.ladoB = b;
			} 
	}

	public int getLadoC() {
		return ladoC;
	}

	public void setLado(int a,int b ,int c) {
		if (condicaoDeExistencia(a,ladoB, ladoC)) {
			this.ladoA = a;
	   }else
			if(condicaoDeExistencia(ladoA, b , ladoC)) {
				this.ladoB = b;
			} else
				if (condicaoDeExistencia(ladoA, ladoB, c))
					this.ladoC = c;
	}
	
	public String toString() {
		
		return this.ladoA + ", " + this.ladoB + " ," + this.ladoC ;
		
	}
	
	public double perimetro() {
		int p = ladoA + ladoB + ladoC;
		return p;
	}
	
	public double area () {
		double s = perimetro() / 2 ;
		float a =  (float) Math.sqrt(s * (s - ladoA) * (s - ladoB) * ( s - ladoC));
		return a;
		
	}
	
}
