
public class Uno implements IFabricaDeCarros {
	private String cor;
	private String fabricante;
	
	public Uno() {
		this.cor = "Vermelho";
		this.fabricante = "Fiat";
	}
	

	public String cor(String cor) {
		return this.cor = cor;
		
	}

	public String fabricante(String fabricante) {
		return this.fabricante = fabricante;
	}
	
	public String toString() {
		return "(" + this.cor + ", " + this.fabricante + ")";
	}

}
