package single;

public class Impressao {
	private static Impressao instance[];

	private static int i;

	private static int MAXIMO = 5;
	
	private String tela;
		
	private Impressao() {
		
	}
	
	public static Impressao getInstance(){
		
		//LAZY		
		if(instance == null) {
			
			instance = new Impressao[MAXIMO];
			
			criarImpressoras();
			
		}
		i = i + 1;
		
		if(i == MAXIMO) {
			i = 0;
		}
		
		return instance[i];		
	}

	private static void criarImpressoras() {
		for(int j = 0; j < MAXIMO ; j++) {
			instance[j] = new Impressao();
		}		
	}

	public void imprimir(String tela) {
		System.out.println(tela);
		
	}
}


