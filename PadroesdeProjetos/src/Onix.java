
public class Onix implements IFabricaDeCarros {
	private String cor;
	private String fabricante;
	
	public Onix() {
		this.cor = "Cinza";
		this.fabricante = "Chevrolet";
	}
	
	public String cor(String cor) {
		return this.cor = cor;
		
	}

	public String fabricante(String fabricante) {
		return this.fabricante = fabricante;
	}
	
	public String toString() {
		return "(" + this.cor + ", " + this.fabricante + ")";
	}

}
