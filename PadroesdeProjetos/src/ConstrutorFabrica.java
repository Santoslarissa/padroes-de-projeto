
public class ConstrutorFabrica implements IConstrucaoFabrica{
	
	public IFabricaDeCarros criar(String nomeCarro) {
		
		if (nomeCarro.equals("onix")) {
			return new Onix();
		}
		if (nomeCarro.equals("uno")) {
			return new Uno();
		}
		if (nomeCarro.equals("celta")) {
			return new Celta();
		}
		if (nomeCarro.equals("civic")) {
			return new Civic();
		}
		return null;
	}

}
