
public class Celta implements IFabricaDeCarros {
	private String cor;
	private String fabricante;
	
	public Celta() {
		this.cor = "Verde";
		this.fabricante = "Chevrolet";
	}
	
	public String cor(String cor) {
		return this.cor = cor;
		
	}

	public String fabricante(String fabricante) {
		return this.fabricante = fabricante;
	}
	
	public String toString() {
		return "(" + this.cor + ", " + this.fabricante + ")";
	}

}
