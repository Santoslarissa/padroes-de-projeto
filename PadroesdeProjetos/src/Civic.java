
public class Civic implements IFabricaDeCarros {
	private String cor;
	private String fabricante;
	
	public Civic() {
		this.cor = "Preto";
		this.fabricante = "Honda";
	}
	public String cor(String cor) {
		return this.cor = cor;
		
	}

	public String fabricante(String fabricante) {
		return this.fabricante = fabricante;
	}
	
	public String toString() {
		return "(" + this.cor + ", " + this.fabricante + ")";
	}

}
