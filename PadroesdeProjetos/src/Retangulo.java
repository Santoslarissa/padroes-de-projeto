
public class Retangulo implements IFiguraBiDimensional {

		private int comprimento;
		private int largura;
		
		private boolean condicaoExistencia(int c, int l) {
			return c > 0 && l > 0;
		}
		
		public Retangulo() {
			this.comprimento = 1;
			this.largura = 2;
		}

		public Retangulo(int comprimento, int largura) {
			
			if (!condicaoExistencia(comprimento, largura)) {
				throw new RuntimeException("Imposs�vel construir ret�ngulo.");
			}
			
			this.comprimento = comprimento;
			this.largura = largura;
		}

		public int getComprimento() {
			return comprimento;
		}

		public void setComprimentoeLargura(int comprimento) {
			if(condicaoExistencia(comprimento, largura)) {
				this.comprimento = comprimento;
			}
		}
		
		public void setComprimentoeLargura(int comprimento,int largura) {
			if(condicaoExistencia(comprimento, largura)) {
				this.comprimento = comprimento;
			} else
				if(condicaoExistencia(comprimento, largura)) {
					this.largura = largura;
				} 
		}

		public int getLargura() {
			return largura;
		}

		public void setLarguraeComprimento(int largura) {
			if(condicaoExistencia(comprimento, largura)) {
				this.largura = largura;
			}
		}
		
		public void setLarguraeComprimento(int largura,int comprimento) {
			if(condicaoExistencia(comprimento, largura)) {
				this.largura = largura;
			} else
				if(condicaoExistencia(comprimento, largura)) {
					this.comprimento = comprimento;
				} 
		}

		public double perimetro() {
			return comprimento + comprimento + largura + largura;
		}

		public double area() {
			return comprimento * largura;
		}

		@Override
		public String toString() {
			return "(" + this.comprimento + ", " + this.comprimento + ", " + this.largura + ", " + this.largura + ")";
		}

	}
